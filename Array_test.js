const items = [1, 2, 3, 4, 5, 5];
const items1 = [1, [2], [[3]], [[[4]]]];

const {each, map, reduce, find, filter, flatten} = require('./Arrays');
//each
each(items, function cb (element, i) { 
    console.log(element,i)
})

//map
console.log(map(items, (value) => value + 10))

//reduce
console.log(reduce(items, (n ,m) => n + m))

//find
console.log(find(items, (a) => a % 2 === 0))

//filter
console.log('Output for filter method is '+filter(items, (element) => element%5 === 0))

//flatten
console.log('Resultant flatten array is ' +flatten(items1))

