//forEach
function each(elements, cb){
    for(let i=0; i<elements.length; i++){
         cb(elements[i],i);
    }
}

//map
function map(elements, cb){
    l1=[]
    for(let i=0; i<elements.length; i++){
        let temp = cb(elements[i], i)
        l1.push(temp)
    }
    return l1
}

//reduce 
function reduce(elements, cb , startingValue){
    let sum = 0
    if(startingValue){
        for ( let i = 0 ; i< elements.length ; i++)
            sum = cb(elements[i], sum , startingValue) 
        return sum+startingValue
    }
    else { 
        for ( let i = 0 ; i< elements.length ; i++)
            sum = cb(elements[i], sum , startingValue)
        return sum
    } 
}

//find 
function find(elements, cb) {
    for (let i = 0 ; i< elements.length ; i++) {
        if (cb(elements[i])) 
            return elements[i];
    }
    return undefined;
}

// created filter function
function filter(elements, cb) {
    arr = []
    for (let  i=0 ; i< elements.length; i++){
        if (cb(elements[i]))
            arr.push(elements[i])
}
return arr
}

// created flatten function
function flatten(items) {
    let temp=[]
   function flattenMethod (items){
       for(let i = 0; i < items.length; i++) {
           if(Array.isArray(items[i])){
           flattenMethod(items[i])
           }
           else {
           temp.push(items[i]) 
           }
       }  
   } 
   flattenMethod(items)
   return temp
}

module.exports = { each, map, reduce, find, filter, flatten}
